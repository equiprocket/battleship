﻿using System;
using System.Windows.Controls;

namespace BattleShip.ViewModel
{
    /// <summary>
    /// Class switcher
    /// </summary>
    public static class Switcher
    {
        /// <summary>
        /// current frame
        /// </summary>
        public static Frame CurrentFrame;

        /// <summary>
        /// switch
        /// </summary>
        /// <param name="newPage"></param>
        public static void Switch(string newPage)
        {
            CurrentFrame.Source = new Uri($"../../View/{newPage}.xaml", UriKind.Relative);
        }
    }
}
