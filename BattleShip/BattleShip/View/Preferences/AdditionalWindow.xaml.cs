﻿namespace BattleShip.View.Preferences
{
    /// <summary>
    /// Interaction logic for PreferencesWindow.xaml
    /// </summary>
    public partial class AdditionalWindow
    {
        /// <summary>
        /// Initializa Additional Window
        /// </summary>
        public AdditionalWindow()
        {
            InitializeComponent();
        }
    }
}
