﻿using System.Linq;
using System.Windows;
using BattleShip.ViewModel;

namespace BattleShip.View
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow
    {
        private readonly BattleShipWindow _battleShipWindow;
        /// <summary>
        /// Game view model
        /// </summary>
        public readonly GameViewModel GameViewModel;

        /// <summary>
        /// Human attack made
        /// </summary>
        public static bool HumanAttackMade = false;
        /// <summary>
        /// Game started
        /// </summary>
        public static bool GameStarted = false;

        /// <summary>
        /// Initialize Game Window
        /// </summary>
        /// <param name="playerName"></param>
        /// <param name="playerAvatar"></param>
        public GameWindow(string playerName, string playerAvatar)
        {
            InitializeComponent();

            GameViewModel = new GameViewModel(playerName, playerAvatar,
                ShipBoard, HitBoard, HumanPlayerShips, ComputerPlayerShips,
                NotificationPanel);

            DataContext = GameViewModel;

            foreach (var window in Application.Current.Windows.OfType<BattleShipWindow>())
            {
                _battleShipWindow = window;
            }

            Closing += GameViewModel.OnWindowClosing;

            Switcher.CurrentFrame = _battleShipWindow?.GameWindowFrame;
        }

        private void GameWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            GameViewModel.IsReady = true;
            GameViewModel.GenerateDesign();
        }

        private void NewGame_OnClick(object sender, RoutedEventArgs e)
        {
            var battleShipWindow = new BattleShipWindow();
            battleShipWindow.Show();
            Switcher.Switch("NewGamePage");
            Close();
        }

        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            var battleShipWindow = new BattleShipWindow();
            battleShipWindow.Show();
            Switcher.Switch("OnePlayerGameSetup");
            Close();
        }
    }
}
