﻿using System.Windows;

namespace BattleShip.View.Controls
{
    /// <summary>
    /// Interaction logic for GameStatusControl.xaml
    /// </summary>
    public partial class GameStatusControl
    {
        /// <summary>
        /// Game Status Control Constructor
        /// </summary>
        public GameStatusControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Human player sunk ship
        /// </summary>
        public static readonly DependencyProperty HumanPlayerSunkShipsProperty = 
            DependencyProperty.Register("HumanPlayerSunkShips",
                typeof(int), typeof(GameStatusControl), 
                new PropertyMetadata(5)
            );

        /// <summary>
        /// current game status
        /// </summary>
        public static readonly DependencyProperty CurrentGameStatusProperty =
            DependencyProperty.Register("CurrentGameStatus",
                typeof(string), typeof(GameStatusControl),
                new PropertyMetadata(string.Empty)
            );

        /// <summary>
        /// computer player sunk ship
        /// </summary>
        public static readonly DependencyProperty ComputerPlayerSunkShipsProperty =
            DependencyProperty.Register("ComputerPlayerSunkShips",
                typeof(int), typeof(GameStatusControl),
                new PropertyMetadata(5)
            );

        /// <summary>
        /// human player sunk ship
        /// </summary>
        public int HumanPlayerSunkShips
        {
            get { return (int) GetValue(HumanPlayerSunkShipsProperty); }
            set { SetValue(HumanPlayerSunkShipsProperty, value); }
        }

        /// <summary>
        /// Current game status
        /// </summary>
        public string CurrentGameStatus
        {
            get { return GetValue(CurrentGameStatusProperty).ToString(); }
            set { SetValue(CurrentGameStatusProperty, value); }
        }

        /// <summary>
        /// computer player sunk ship
        /// </summary>
        public int ComputerPlayerSunkShips
        {
            get { return (int) GetValue(ComputerPlayerSunkShipsProperty); }
            set { SetValue(ComputerPlayerSunkShipsProperty, value); }
        }
    }
}
