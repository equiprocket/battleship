﻿using System;
using System.Windows.Media.Imaging;

namespace BattleShip.Model.Cached
{
    /// <summary>
    /// Class Image
    /// </summary>
    public class Image
    {
        /// <summary>
        /// BitmapImage
        /// </summary>
        public BitmapImage Source { get; }

        /// <summary>
        /// Image
        /// </summary>
        /// <param name="imagePath"></param>
        public Image(string imagePath)
        {
            Source = new BitmapImage(new Uri($"../../Resources/images/{imagePath}.png", UriKind.Relative));
        }
    }
}
