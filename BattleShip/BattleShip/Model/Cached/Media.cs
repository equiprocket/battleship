﻿using System.Media;

namespace BattleShip.Model.Cached
{
    /// <summary>
    /// Class media
    /// </summary>
    public class Media
    {
        /// <summary>
        /// Sounds
        /// </summary>
        public SoundPlayer Sound { get; }

        /// <summary>
        /// Media
        /// </summary>
        /// <param name="mediaPath"></param>
        public Media(string mediaPath)
        {
            Sound = new SoundPlayer
            {
                SoundLocation = $"../../Resources/audio/{mediaPath}.wav"
            };
        }
    }
}