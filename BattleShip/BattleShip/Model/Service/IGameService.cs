﻿using BattleShip.BattleShipService;

namespace BattleShip.Model.Service
{
    /// <summary>
    /// Interface Game service
    /// </summary>
    public interface IGameService
    {

        /// <summary>
        /// Add player to Players Table 
        /// </summary>
        /// <param name="player"></param>
        void AddPlayer(Player player);

        
        /// <summary>
        ///  Get information about player from database
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Player GetPlayerByName(string name);

        // 
        /// <summary>
        ///Add game statistic after game finished 
        /// </summary>
        /// <param name="gameStatistic"></param>
        void AddStatistic(GameStatistic gameStatistic);

        // 
        /// <summary>
        /// Get statistics about previous succeded games
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        GameStatistic[] GetStatisticsByPlayerName(string name);
    }
}