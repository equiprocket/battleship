﻿using BattleShip.BattleShipService;

namespace BattleShip.Model.Service
{
    /// <summary>
    /// Class BattleShip service
    /// </summary>
    public class BattleShipService : IGameService
    {
        private readonly IGameStatisticsService _gameStatisticsService;
        private readonly IPlayerService _playerService;

        /// <summary>
        /// Constructor
        /// </summary>
        public BattleShipService()
        {
            _gameStatisticsService = new GameStatisticsServiceClient();
            _playerService = new PlayerServiceClient();
        }


        /// <summary>
        /// // Add player to Players Table
        /// </summary>
        /// <param name="player"></param>
        public void AddPlayer(Player player)
        {
            _playerService.AddPlayer(player);
        }


        /// <summary>
        ///  // Get information about player from database
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Player GetPlayerByName(string name)
        {
            return _playerService.GetPlayerByName(name);
        }


        /// <summary>
        ///  Add game statistic after game finished
        /// </summary>
        /// <param name="gameStatistic"></param>
        public void AddStatistic(GameStatistic gameStatistic)
        {
            _gameStatisticsService.AddStatistic(gameStatistic);
        }

        /// <summary>
        ///Get statistics about previous succeded games
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public GameStatistic[] GetStatisticsByPlayerName(string name)
        {
            return _gameStatisticsService.GetStatisticsByPlayerName(name);
        }
    }
}
