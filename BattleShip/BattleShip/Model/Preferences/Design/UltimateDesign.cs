﻿using System.Windows.Media.Imaging;
using BattleShip.Model.Cached;

namespace BattleShip.Model.Preferences.Design
{
    /// <summary>
    /// Class Ultimate Design
    /// </summary>
    public class UltimateDesign : IAppDesign
    {
        private readonly MediaFactory _mediaFactory;

        /// <summary>
        /// Constructor
        /// </summary>
        public UltimateDesign()
        {
            _mediaFactory = MediaFactory.Init;
        }

        /// <summary>
        /// 
        /// </summary>
        public BitmapImage PlayerPanelImage => _mediaFactory.GetImage("ultimatePlayerPanelBackground");

        /// <summary>
        /// 
        /// </summary>
        public BitmapImage NotificationPanelImage => _mediaFactory.GetImage("ultimateNotificationPanelBackground");
        
        /// <summary>
        /// 
        /// </summary>
        public BitmapImage ShipboardImage => _mediaFactory.GetImage("boardUltimate");
        
        /// <summary>
        /// 
        /// </summary>
        public BitmapImage BackgroundImage => _mediaFactory.GetImage("setupBackground");
    }
}
