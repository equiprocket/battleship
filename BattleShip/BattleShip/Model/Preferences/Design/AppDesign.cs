﻿using System.Windows.Media.Imaging;

namespace BattleShip.Model.Preferences.Design
{
    /// <summary>
    /// App Design
    /// </summary>
    public enum AppDesign
    {
        /// <summary>
        /// Standart
        /// </summary>
        Standart,
        /// <summary>
        /// Ultimate
        /// </summary>
        Ultimate
    }

    /// <summary>
    /// Interface App design
    /// </summary>
    public interface IAppDesign
    {
        /// <summary>
        /// player panel image
        /// </summary>
        BitmapImage PlayerPanelImage { get; }
        /// <summary>
        /// notification planel image
        /// </summary>
        BitmapImage NotificationPanelImage { get; }
        /// <summary>
        /// Ship board image
        /// </summary>
        BitmapImage ShipboardImage { get; }
        /// <summary>
        /// background image
        /// </summary>
        BitmapImage BackgroundImage { get; }
    }
}
