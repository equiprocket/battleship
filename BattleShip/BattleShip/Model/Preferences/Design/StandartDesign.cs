﻿using System.Windows.Media.Imaging;
using BattleShip.Model.Cached;

namespace BattleShip.Model.Preferences.Design
{
    /// <summary>
    /// Class Standart Desing
    /// </summary>
    public class StandartDesign : IAppDesign
    {
        private readonly MediaFactory _mediaFactory;

        /// <summary>
        /// Standart Design Constructor
        /// </summary>
        public StandartDesign()
        {
            _mediaFactory = MediaFactory.Init;
        }

        /// <summary>
        /// 
        /// </summary>
        public BitmapImage PlayerPanelImage => _mediaFactory.GetImage("playerPanelBackground");

        /// <summary>
        /// 
        /// </summary>
        public BitmapImage NotificationPanelImage => _mediaFactory.GetImage("notificationPanelBackground");

        /// <summary>
        /// 
        /// </summary>
        public BitmapImage ShipboardImage => _mediaFactory.GetImage("board");

        /// <summary>
        /// 
        /// </summary>
        public BitmapImage BackgroundImage => _mediaFactory.GetImage("setupBackground");
    }
}
