﻿using System.Windows;
using BattleShip.Model.Preferences;
using BattleShip.Properties;

namespace BattleShip
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        /// <summary>
        /// On Startup method
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            AppSettings.BackgroundMusic();
        }
    }
}
