﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using GameEngine.Annotations;

namespace GameEngine.Attacks
{
    /// <summary>
    /// Class attack
    /// </summary>
    public sealed class Attack : INotifyPropertyChanged
    {
        private AttackResult _attackResult;

        /// <summary>
        /// Result attack
        /// </summary>
        public AttackResult Result
        {
            get { return _attackResult; }
            set
            {
                _attackResult = value;
                OnPropertyChanged(nameof(Result));
            }
        }

        /// <summary>
        /// Attack contructor
        /// </summary>
        public Attack()
        {
            _attackResult = AttackResult.Unknown;
        }

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}