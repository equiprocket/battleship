﻿namespace GameEngine.Attacks
{
    /// <summary>
    /// Attack Result
    /// </summary>
    public enum AttackResult
    {
        /// <summary>
        /// This is default value
        /// </summary>
        Unknown = 0,    // 
        /// <summary>
        /// If attacked is fails
        /// </summary>
        Miss = -1,      // 
        /// <summary>
        /// If user hit one of the ship part
        /// </summary>
        Hit = 1         // 
    }
}