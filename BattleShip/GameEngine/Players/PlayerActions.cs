﻿namespace GameEngine.Players
{
    /// <summary>
    /// Class player actions
    /// </summary>
    public class PlayerActions
    {
        /// <summary>
        /// turns
        /// </summary>
        public int Turns { get; set; }
        /// <summary>
        /// misses
        /// </summary>
        public int Misses { get; set; }
        /// <summary>
        /// hits
        /// </summary>
        public int Hits { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public PlayerActions()
        {
            Turns = 0;
            Misses = 0;
            Hits = 0;
        }
    }
}