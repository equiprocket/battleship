﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameEngine.Attacks;
using GameEngine.Helpers;
using GameEngine.Ships;

namespace GameEngine.Players
{
    /// <summary>
    /// Class ComputerPlayer
    /// </summary>
    public class ComputerPlayer : IPlayer
    {
        /// <summary>
        /// Player name
        /// </summary>
        public string PlayerName { get; set; }
        /// <summary>
        /// Player avatar
        /// </summary>
        public string PlayerAvatar { get; set; }


        /// <summary>
        ///  Player ships
        /// </summary>
        public Ship[] Ships { get; set; }


        /// <summary>
        /// Player attacks
        /// </summary>
        public Attack[,] Attacks { get; set; }


        /// <summary>
        ///  Max coordinates
        /// </summary>
        public Coordinate MaxCoordinates { get; set; }


        /// <summary>
        /// Ship placement helper
        /// </summary>
        protected ShipPlacementHelper ShipPlacementHelper;

        /// <summary>
        /// // Initialize Player computer
        /// </summary>
        /// <param name="playerName"></param>
        /// <param name="playerAvatar"></param>
        protected ComputerPlayer(string playerName, string playerAvatar)
        {
            PlayerName = playerName;
            PlayerAvatar = playerAvatar;
        }

        /// <summary>
        /// 
        /// </summary>
        public ComputerPlayer()
        {
        }


        /// <summary>
        /// Initialize Player
        /// </summary>
        /// <param name="maxCoordinates"></param>
        /// <param name="startingShips"></param>
        public virtual void Initialize(Coordinate maxCoordinates, Ship[] startingShips)
        {
            Ships = startingShips;

            if (Attacks == null)
            {
                Attacks = new Attack[maxCoordinates.X + 1, maxCoordinates.Y + 1];

                for (var x = 0; x <= maxCoordinates.X; x++)
                    for (var y = 0; y <= maxCoordinates.Y; y++) 
                        Attacks[x, y] = new Attack();
            }

            MaxCoordinates = maxCoordinates;
            ShipPlacementHelper = new ShipPlacementHelper(maxCoordinates);
        }


        /// <summary>
        /// Check if user is ready to go :)
        /// </summary>
        /// <returns></returns>
        public virtual bool IsReady()
        {
            return true;
        }


        /// <summary>
        /// // Place all ships
        /// </summary>
        public virtual void PlaceShips()
        {
            var placedShips = new List<Ship>();
            for (var i = 0; i < Ships.Length; i++)
            {
                Ships[i] = PlaceShip(Ships[i], placedShips);
                placedShips.Add(Ships[i]);
            }
        }


        /// <summary>
        /// Place current ship
        /// </summary>
        /// <param name="ship"></param>
        /// <param name="placedShips"></param>
        /// <returns></returns>
        private Ship PlaceShip(Ship ship, List<Ship> placedShips)
        {
            if (ship.Sections.Count(section => section.ShipCoordinate.X != -100 && section.ShipCoordinate.Y != -100) ==
                ship.Sections.Length)
                return ship;

            var random = new Random();
            var isSafePlacement = false;

            while (!isSafePlacement)
            {
                isSafePlacement = true;
                var x = random.Next(MaxCoordinates.X);
                var y = random.Next(MaxCoordinates.Y);

                var secMod = 0;
                var xMod = 0;
                var yMod = 0;

                // Pick a random direction
                switch (random.Next(3))
                {
                    case 0:
                        yMod = 1; // Up
                        break;

                    case 1:
                        yMod = -1; // Down
                        break;

                    case 2:
                        xMod = -1; // Left
                        break;

                    case 3:
                        xMod = 1; // Right
                        break;
                }

                // Place Ship
                foreach (var section in ship.Sections)
                {
                    section.ShipCoordinate = new Coordinate(x + secMod * xMod, y + secMod * yMod);
                    secMod++;
                }

                // Check for safe placement
                if (ShipPlacementHelper.IsInvalidPlacement(ship) ||
                    ShipPlacementHelper.PlacementCreatesConflict(ship, placedShips))
                {
                    isSafePlacement = false;
                }
            }

            return ship;
        }


        /// <summary>
        ///  Player attack
        /// </summary>
        /// <returns></returns>
        public virtual Coordinate Attack()
        {
            return CalcRandomAttack();
        }


        /// <summary>
        ///  Update list of attacks
        /// </summary>
        /// <param name="lastAttack"></param>
        /// <param name="attackResult"></param>
        /// <param name="sunkShip"></param>
        public virtual void UpdateAttackResults(Coordinate lastAttack, AttackResult attackResult, bool sunkShip)
        {
            Attacks[lastAttack.X, lastAttack.Y].Result = attackResult;
        }


        /// <summary>
        /// If user Win -> send notification
        /// </summary>
        /// <param name="winnerName"></param>
        public virtual void WinnerNotification(string winnerName) { }


        /// <summary>
        /// Set random attack
        /// </summary>
        /// <returns></returns>
        protected Coordinate CalcRandomAttack()
        {
            var random = new Random();
            var attack = new Coordinate(random.Next(MaxCoordinates.X + 1), random.Next(MaxCoordinates.Y + 1));

            while (AttackExists(attack))
            {
                attack = new Coordinate(random.Next(MaxCoordinates.X + 1), random.Next(MaxCoordinates.Y + 1));
            }

            return attack;
        }

     
        /// <summary>
        /// Check if current attack is exists
        /// </summary>
        /// <param name="attack"></param>
        /// <returns></returns>
        private bool AttackExists(Coordinate attack)
        {
            if (Attacks[attack.X, attack.Y].Result != AttackResult.Unknown)
                return true;

            return false;
        }


        /// <summary>
        /// // Calculate attack coordinates
        /// </summary>
        /// <param name="coordinate"></param>
        /// <returns></returns>
        protected List<Coordinate> CalcAdjCoordinates(Coordinate coordinate)
        {
            var adjList = new List<Coordinate>
            {
                new Coordinate(coordinate.X + 1, coordinate.Y),
                new Coordinate(coordinate.X - 1, coordinate.Y),
                new Coordinate(coordinate.X, coordinate.Y + 1),
                new Coordinate(coordinate.X, coordinate.Y - 1)
            };

            adjList.RemoveAll(x => ShipPlacementHelper.IsOutOfBounds(x));
            return adjList;
        }

        // 
        /// <summary>
        /// Update Ui State
        /// </summary>
        /// <param name="currentState"></param>
        public void UpdateUiState(UiState currentState) { }
    }
}