﻿using GameEngine.Attacks;
using GameEngine.Ships;

namespace GameEngine.Players
{
    /// <summary>
    /// Interface player
    /// </summary>
    public interface IPlayer
    {
        /// <summary>
        /// player name
        /// </summary>
        string PlayerName { get; set; }
        /// <summary>
        /// player avatar
        /// </summary>
        string PlayerAvatar { get; }

        // 
        /// <summary>
        /// Player ships
        /// </summary>
        Ship[] Ships { get; }

        // 
        /// <summary>
        /// Player attacks
        /// </summary>
        Attack[,] Attacks { get; set; }

        // 
        /// <summary>
        /// Max coordinates
        /// </summary>
        Coordinate MaxCoordinates { get; set; }

        // 
        /// <summary>
        /// Initialize Player
        /// </summary>
        /// <param name="maxCoords"></param>
        /// <param name="startingShips"></param>
        void Initialize(Coordinate maxCoords, Ship[] startingShips);

        // 
        /// <summary>
        /// Check if user is ready to go :)
        /// </summary>
        /// <returns></returns>
        bool IsReady();

        // 
        /// <summary>
        /// Place all ships
        /// </summary>
        void PlaceShips();

        // 
        /// <summary>
        /// Player attack
        /// </summary>
        /// <returns></returns>
        Coordinate Attack();

        // 
        /// <summary>
        /// Update list of attacks
        /// </summary>
        /// <param name="lastAttack"></param>
        /// <param name="attackResult"></param>
        /// <param name="sunkShip"></param>
        void UpdateAttackResults(Coordinate lastAttack, AttackResult attackResult, bool sunkShip);

        // 
        /// <summary>
        /// If user Win -> send notification
        /// </summary>
        /// <param name="winnerName"></param>
        void WinnerNotification(string winnerName);

        // 
        /// <summary>
        /// Update Ui State
        /// </summary>
        /// <param name="currentState"></param>
        void UpdateUiState(UiState currentState);
    }
}