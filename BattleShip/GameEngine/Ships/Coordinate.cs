﻿namespace GameEngine.Ships
{
    /// <summary>
    /// Class coordinate
    /// </summary>
    public class Coordinate
    {
        /// <summary>
        /// Coord X
        /// </summary>
        public int X { get; set; }
        /// <summary>
        /// Coord Y
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// coordinate
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Coordinate(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}