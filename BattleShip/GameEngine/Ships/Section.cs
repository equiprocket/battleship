﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using GameEngine.Annotations;

namespace GameEngine.Ships
{
    /// <summary>
    /// Class section
    /// </summary>
    public sealed class Section : INotifyPropertyChanged
    {
        private bool _isDamaged;

        /// <summary>
        /// Is damaged
        /// </summary>
        public bool IsDamaged
        {
            get { return _isDamaged;}
            set
            {
                _isDamaged = value;
                OnPropertyChanged(nameof(IsDamaged));
            }
        }

        /// <summary>
        /// Ship coordinate
        /// </summary>
        public Coordinate ShipCoordinate { get; set; }

        /// <summary>
        /// Section
        /// </summary>
        /// <param name="shipCoordinate"></param>
        public Section(Coordinate shipCoordinate)
        {
            _isDamaged = false;
            ShipCoordinate = shipCoordinate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isDamaged"></param>
        /// <param name="shipCoordinate"></param>
        public Section(bool isDamaged, Coordinate shipCoordinate)
        {
            IsDamaged = isDamaged;
            ShipCoordinate = shipCoordinate;
        }

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}