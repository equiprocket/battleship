﻿using System.Linq;

namespace GameEngine.Ships
{
    /// <summary>
    ///  Class Ship
    /// </summary>
    public class Ship
    {
        /// <summary>
        /// name
        /// </summary>
        public string Name { get; private set; }
        private string DisplayName { get; set; }

        //
        /// <summary>
        ///  Ship section
        /// </summary>
        public Section[] Sections { get; }

        // 
        /// <summary>
        /// Ship constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="displayName"></param>
        /// <param name="sections"></param>
        public Ship(string name, string displayName, Section[] sections)
        {
            Name = name;
            DisplayName = displayName;
            Sections = sections;
        }


        /// <summary>
        /// Check if all parts of ship are destroyed
        /// Check if ship is destroyed
        /// </summary>
        /// <returns></returns>
        public bool IsDestroyed()
        {
            return Sections.All(parts => parts.IsDamaged);
        }
    }
}