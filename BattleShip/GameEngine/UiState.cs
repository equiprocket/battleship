﻿using System.ComponentModel;

namespace GameEngine
{
    /// <summary>
    /// State UI
    /// </summary>
    public enum UiState
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("Waiting to Place")]
        WaitingToPlace = 0,

        /// <summary>
        /// waiting place
        /// </summary>
        Placing,

        /// <summary>
        /// waiting attack
        /// </summary>
        [Description("Waiting to Attack")]
        WaitingToAttack,

        /// <summary>
        /// attacking
        /// </summary>
        Attacking,

        /// <summary>
        /// game finished
        /// </summary>
        [Description("Game Finished")]
        GameFinished
    }
}