﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameEngine.Serialization
{
    /// <summary>
    /// Class BattleShipSerializer
    /// </summary>
    public static class BattleShipSerializer
    {
        /// <summary>
        /// Serializer data
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="battleShipData"></param>
        public static void SerializeData(string fileName, object battleShipData)
        {
            var formatter = new BinaryFormatter();
            var fileStream = new FileStream(fileName, FileMode.Create);
            formatter.Serialize(fileStream, battleShipData);
            fileStream.Close();
        }

        /// <summary>
        /// Deserializer
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static object DeserializeData(string fileName)
        {
            var fileStream = new FileStream(fileName, FileMode.Open);
            var formatter = new BinaryFormatter();
            return formatter.Deserialize(fileStream);
        }
    }
}