﻿using System.Collections.Generic;
using System.Linq;
using GameEngine.Attacks;
using GameEngine.Players;
using GameEngine.Ships;

namespace GameEngine.Serialization
{
    /// <summary>
    /// Game deserializer
    /// </summary>
    public class GameDeserializer
    {
        #region Members
        /// <summary>
        /// player name
        /// </summary>
        public readonly string PlayerName;
        /// <summary>
        /// player avatar
        /// </summary>
        public readonly string PlayerAvatar;
        /// <summary>
        /// attacks
        /// </summary>
        public readonly Attack[,] Attacks;
        /// <summary>
        /// Ships
        /// </summary>
        public readonly Ship[] Ships;
        /// <summary>
        /// Max coordinate
        /// </summary>
        public readonly Coordinate MaxCoordinates;
        /// <summary>
        /// computer player
        /// </summary>
        public readonly ComputerPlayer ComputerPlayer;
        /// <summary>
        /// Current state
        /// </summary>
        public readonly UiState CurrentState;
        #endregion // Members

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="battleShipData"></param>
        public GameDeserializer(BattleShipData battleShipData)
        {
            PlayerName = battleShipData.PlayerName;
            PlayerAvatar = battleShipData.PlayerAvatar;

            MaxCoordinates = GetConvertedCoordinates(battleShipData.MaxCoordinates);
            Attacks = GetConvertedAttacks(battleShipData.Attacks, battleShipData.MaxCoordinates);
            Ships = GetConvertedShips(battleShipData.Ships);

            ComputerPlayer = new ComputerPlayer
            {
                PlayerName = battleShipData.ComputerPlayer.PlayerName,
                PlayerAvatar = battleShipData.ComputerPlayer.PlayerAvatar,
                Attacks =
                    GetConvertedAttacks(battleShipData.ComputerPlayer.Attacks,
                        battleShipData.ComputerPlayer.MaxCoordinates),
                Ships = GetConvertedShips(battleShipData.ComputerPlayer.Ships),
                MaxCoordinates = GetConvertedCoordinates(battleShipData.ComputerPlayer.MaxCoordinates)
            };

            CurrentState = battleShipData.CurrentState;
        }
        #endregion // Constructor
        
        #region Methods
        private static Attack[,] GetConvertedAttacks(AttackSerializable[,] playerAttacks, CoordinateSerializable maxCoordinates)
        {
            var attacks = new Attack[maxCoordinates.X + 1, maxCoordinates.Y + 1];

            for (var i = 0; i <= maxCoordinates.X; i++)
            {
                for (var j = 0; j <= maxCoordinates.Y; j++)
                {
                    attacks[i, j] = new Attack
                    {
                        Result = playerAttacks[i, j].Result
                    };
                }
            }

            return attacks;
        }

        private static Coordinate GetConvertedCoordinates(CoordinateSerializable maxCoordinates)
        {
            return new Coordinate(maxCoordinates.X, maxCoordinates.Y);
        }

        private static Ship[] GetConvertedShips(IEnumerable<ShipSerializable> ships)
        {
            return
                (from ship in ships
                 let sections = ship.Sections.Select(section =>
                     new Section(
                         section.IsDamaged, new Coordinate(
                             section.ShipCoordinate.X,
                             section.ShipCoordinate.Y
                             )
                         )
                     ).ToArray()
                 select new Ship(
                     ship.Name,
                     ship.Name,
                     sections
                     )
                    ).ToArray();
        }
        #endregion // Methods
    }
}