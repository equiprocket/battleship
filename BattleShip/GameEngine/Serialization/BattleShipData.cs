﻿using System;
using GameEngine.Attacks;

namespace GameEngine.Serialization
{
    /// <summary>
    /// Class battleshipdata
    /// </summary>
    [Serializable]
    #region BattleShipData
    public class BattleShipData
    {
        /// <summary>
        /// player name
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// player avatar
        /// </summary>
        public string PlayerAvatar { get; set; }

        /// <summary>
        /// ships
        /// </summary>
        public ShipSerializable[] Ships { get; set; }

        /// <summary>
        /// attacks
        /// </summary>
        public AttackSerializable[,] Attacks { get; set; }

        /// <summary>
        /// max coordinate
        /// </summary>
        public CoordinateSerializable MaxCoordinates { get; set; }

        /// <summary>
        /// computer player
        /// </summary>
        public ComputerPlayerSerializable ComputerPlayer { get; set; }

        /// <summary>
        /// current state
        /// </summary>
        public UiState CurrentState { get; set; }
    }
    #endregion // BattleShipData

    /// <summary>
    /// ComputerPlayerSerializable
    /// </summary>
    [Serializable]
    #region ComputerPlayer
    public class ComputerPlayerSerializable
    {
        /// <summary>
        /// 
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PlayerAvatar { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ShipSerializable[] Ships { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AttackSerializable[,] Attacks { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CoordinateSerializable MaxCoordinates { get; set; }
    }
    #endregion // ComputerPlayer

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    #region Attack
    public class AttackSerializable
    {
        /// <summary>
        /// 
        /// </summary>
        public AttackResult Result { get; set; }
    }
    #endregion // Atack

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    #region Ship
    public class ShipSerializable
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public SectionSerializable[] Sections { get; set; }
    }
    #endregion // Ship

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    #region Section
    public class SectionSerializable
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IsDamaged { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CoordinateSerializable ShipCoordinate { get; set; }
    }
    #endregion // Section

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    #region Coordinate
    public class CoordinateSerializable
    {
        /// <summary>
        /// Coord X
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Coord Y
        /// </summary>
        public int Y { get; set; }
    }
    #endregion // Coordinate
}
