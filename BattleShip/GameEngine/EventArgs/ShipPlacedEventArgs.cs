﻿namespace GameEngine.EventArgs
{
    /// <summary>
    /// Ships Placed Event Args
    /// </summary>
    public class ShipsPlacedEventArgs : System.EventArgs { }
}