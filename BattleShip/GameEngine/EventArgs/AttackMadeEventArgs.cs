﻿using GameEngine.Ships;

namespace GameEngine.EventArgs
{
    /// <summary>
    /// Attack Made Event Args
    /// </summary>
    public class AttackMadeEventArgs : System.EventArgs
    {
        /// <summary>
        /// Attack Coordinate
        /// </summary>
        public readonly Coordinate AttackCoordinate;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="attackCoordinate"></param>
        public AttackMadeEventArgs(Coordinate attackCoordinate)
        {
            AttackCoordinate = attackCoordinate;
        }
    }
}