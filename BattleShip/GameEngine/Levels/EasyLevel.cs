﻿using GameEngine.Players;

namespace GameEngine.Levels
{
    /// <summary>
    /// Class Easy Level
    /// </summary>
    public class EasyLevel : ComputerPlayer
    {
        /// <summary>
        /// Easy Level constructor
        /// </summary>
        /// <param name="playerName"></param>
        /// <param name="playerAvatar"></param>
        public EasyLevel(string playerName, string playerAvatar) :
            base(playerName, playerAvatar)
        { }
    }
}