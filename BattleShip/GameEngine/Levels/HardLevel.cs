﻿namespace GameEngine.Levels
{
    /// <summary>
    /// Class Hard Level
    /// </summary>
    public class HardLevel : NormalLevel
    {
        /// <summary>
        /// Hard Level constructor
        /// </summary>
        /// <param name="playerName"></param>
        /// <param name="playerAvatar"></param>
        public HardLevel(string playerName, string playerAvatar) : 
            base(playerName, playerAvatar) { }
    }
}