﻿namespace GameEngine.Levels
{
    /// <summary>
    /// Difficulty
    /// </summary>
    public enum Difficulty
    {
        /// <summary>
        /// description in Levels/EasyLevel.cs file
        /// </summary>
        Easy,       // 
        /// <summary>
        /// description in Levels/NormalLevel.cs file
        /// </summary>
        Normal,     // 
        /// <summary>
        /// description in Levels/HardLevel.cs file
        /// </summary>
        Hard        // 
    }
}