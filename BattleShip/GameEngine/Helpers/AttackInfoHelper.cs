﻿using GameEngine.Attacks;
using GameEngine.Ships;

namespace GameEngine.Helpers
{
    /// <summary>
    /// Class Attactk information helper
    /// </summary>
    public class AttackInfoHelper
    {
        /// <summary>
        /// Coodinates
        /// </summary>
        public readonly Coordinate Coordinate;
        private AttackResult _attackResult;
        private bool _sunkShip;

        /// <summary>
        /// Attack info helper constructor
        /// </summary>
        /// <param name="coordinate"></param>
        /// <param name="attackResult"></param>
        /// <param name="sunkShip"></param>
        public AttackInfoHelper(Coordinate coordinate, AttackResult attackResult, bool sunkShip)
        {
            Coordinate = coordinate;
            _attackResult = attackResult;
            _sunkShip = sunkShip;
        }
    }
}